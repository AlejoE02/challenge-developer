const mysql = require("mysql");
const Sequelize = require("sequelize");
require("dotenv").config();

// console.log(process.env.HOST)
var sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PWD,
  // 'developer',
  {
    // host: 'postgres',
    // port: 5432,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
  }
);
sequelize
  .authenticate()
  .then(() => {
    console.log("Database is connected");
  })
  .catch((err) => {
    console.log(`Database conection error => ${err}`);
  });
module.exports = sequelize;

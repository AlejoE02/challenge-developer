const sequelize = require("./db.js");
const Sequelize = require('sequelize');
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.locationModel = require("./location.js")(sequelize, Sequelize);
module.exports = db;
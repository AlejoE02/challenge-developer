require('dotenv').config();

const server = require('./server');

server.listen(3000, () => console.log(`Server is live at localhost:3000`));

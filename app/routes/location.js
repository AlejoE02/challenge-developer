const { Router } = require('express');

const router = Router();

const location = require("../controllers/locationController");

router.post('/register', location.store)
router.post('/getAll', location.getAll)

router.get('/', (req, res) => res.send('Welcome'))

module.exports = router

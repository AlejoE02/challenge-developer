const { Router } = require("express");

const router = Router();

router.get("/", (req, res) => res.send("Welcome"));

const location = require("../controllers/locationController");

router.post("/register", location.store);
router.get("/getAll", location.getAll);
router.get("/get/:locationId", location.getLocationById);
router.put("/location/:locationId", location.updateLocation);
router.delete('/location/:locationId', location.deleteLocation);

module.exports = router;

const express = require("express");
const routes = require("../routes");
const bodyParser = require('body-parser')

const server = express();

server.use(bodyParser.json({limit: '900mb'}))
server.use(bodyParser.urlencoded({limit: '900mb', extended: true, parameterLimit:900000}))

server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});
server.use("/api", routes);

module.exports = server;

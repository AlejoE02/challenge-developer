const sql = require("../models/db.js");
const models = require("../models");
const sequelize = require("sequelize");
const LocationModel = models.locationModel;
const Op = require("sequelize").Op;

const store = async (req, res) => {
  try {

    //Valida que la locacion sea unica por email
    console.log("Parametros", req.body);
    const email = await models.locationModel.findOne({
      where: { email: req.body.email },
    });

    if (email) {
      return res.status(422).json(
        {
          message: "Email is already exists",
        }
      );
    } else {
      const location = await models.locationModel.create(req.body);
      return res.status(201).json({
        location,
      });
    }
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
};

const getAll = async (req, res) => {
  try {
    const locations = await models.locationModel.findAll({});
    return res.status(200).json({ locations });
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const getLocationById = async (req, res) => {
  try {
    const { locationId } = req.params;
    const location = await models.locationModel.findOne({
      where: { id: locationId },
    });
    if (location) {
      return res.status(200).json({ location });
    }
    return res
      .status(404)
      .send("Location with the specified ID does not exists");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const updateLocation = async (req, res) => {
  try {
    const { locationId } = req.params;
    const [updated] = await models.locationModel.update(req.body, {
      where: { id: locationId },
    });
    if (updated) {
      const updatedLocation = await models.locationModel.findOne({
        where: { id: locationId },
      });
      return res.status(200).json({ post: updatedLocation });
    }
    throw new Error("Location not found");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const deleteLocation = async (req, res) => {
  try {
    const { locationId } = req.params;
    const deleted = await models.locationModel.destroy({
      where: { id: locationId },
    });
    if (deleted) {
      return res.status(204).send("Location deleted");
    }
    throw new Error("Location not found");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

module.exports = {
  store,
  getAll,
  getLocationById,
  updateLocation,
  deleteLocation,
};

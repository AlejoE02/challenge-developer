# challenge-developer

Full stack developer challenge

## Comenzando

Las siguientes instrucciones le permitiran descargar el proyecto y configurarlo en su maquina local

### Instalación

Descargar el repositorio

```
git clone https://gitlab.com/AlejoE02/challenge-developer.git
```

Entre a la carpeta que se acaba de descargar

```
cd challenge-developer
```

Inicializar el docker-compose (puede ser necesario hacerlo con sudo)

```
docker-compose up -d --build
```

Luego de descargar y subir el docker, compruebe que esten corriendo correctamente, debera ver un docker de node y uno de postgres corriendo

```
docker ps
```

Una vez esten corriendo los docker, es necesario entrar al docker de node, ejecute el siguiente comando:

```
docker exec -it challenge-node bash
```

Dentro del docker de node, correr las migraciones de la base

```
npx sequelize-cli db:migrate
```


En caso de error verificar que la base se haya conectado con node, puede ver los logs del docker de node con un mensaje del estado de la conexión a la base (Database is connected):

```
docker logs -f challenge-node
```




